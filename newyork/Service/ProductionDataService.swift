//
//  ProductionDataService.swift
//  newyork
//
//  Created by Francisco Perez on 29/06/23.
//

import Foundation
import Combine


class ProductionDataService {
    //singleton
    static let instance = ProductionDataService()
    
    let url:URL = URL(string:"https://jsonplaceholder.typicode.com/posts")!
    
    
    func getData() -> AnyPublisher<[Posts],Error> {

        URLSession.shared.dataTaskPublisher(for: url)
            .map({ $0.data })
            .decode(type: [Posts].self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
        
    }
}
