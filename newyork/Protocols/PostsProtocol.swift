//
//  PostsProtocol.swift
//  newyork
//
//  Created by Francisco Perez on 28/06/23.
//

import Foundation


protocol PostsProtocol {
    
    func getData() -> [Posts]
    
}
