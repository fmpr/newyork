//
//  PostsViewModel.swift
//  newyork
//
//  Created by Francisco Perez on 28/06/23.
//

import Foundation
import Combine



class PostsViewModel:ObservableObject {

    @Published var dataArray:[Posts] = []
    var cancellables = Set<AnyCancellable>()
    
    init() {
     loadPosts()
    }
    
    
    private func loadPosts() {
        ProductionDataService().getData()
            .sink { _ in
                
            } receiveValue: { [weak self] returnedPosts in
                self?.dataArray = returnedPosts
            }
            .store(in: &cancellables)

    }

}
