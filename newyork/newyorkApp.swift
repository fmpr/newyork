//
//  newyorkApp.swift
//  newyork
//
//  Created by Francisco Perez on 28/06/23.
//

import SwiftUI

@main
struct newyorkApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
