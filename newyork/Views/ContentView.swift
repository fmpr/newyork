//
//  ContentView.swift
//  newyork
//
//  Created by Francisco Perez on 28/06/23.
//

import SwiftUI

struct ContentView: View {
    
    @StateObject private var vm:PostsViewModel = PostsViewModel()

    var body: some View {
        
        ScrollView {
            VStack {
                ForEach( vm.dataArray) { post in
                    Text(post.title)
                    
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
